# Elixir

## Install Erlang

```
$ brew install erlang
$ cd ~/bin && curl -O https://s3.amazonaws.com/rebar3/rebar3 && chmod +x rebar3
```

## Install Elixir

```
$ brew install elixir
```
