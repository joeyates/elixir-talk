defmodule Crud.Repo.Migrations.CreateThing do
  use Ecto.Migration

  def change do
    create table(:things) do
      add :name, :string
      add :weight, :integer

      timestamps
    end

  end
end
