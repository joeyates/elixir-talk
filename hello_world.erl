-module(hello_world).
-export([say/1]).

say(Args) -> io:format("~p~n", Args).
