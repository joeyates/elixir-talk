-module(fetch_json).

-export([init/0]).

init() ->
  application:ensure_all_started(hackney),
  start().

start() ->
  case fetch() of
    {ok} ->
      start();
    {error} ->
      {error}
  end.

fetch() ->
  Url = <<"https://catalogue.bookrepublic.it/promotions/daily.json">>,
  io:format("Fetching ~p.\n\n", [Url]),
  case hackney:request(get, Url, [], <<>>, []) of
    {ok, 200, _RespHeaders, ClientRef} ->
      {ok, Body} = hackney:body(ClientRef),
      io:format("Body ~p.\n\n", [Body]),
      {ok};
    {ok, StatusCode, _RespHeaders, _ClientRef} ->
      io:format("Error status ~p.\n\n", [StatusCode]),
      {error};
    {error, Reason} ->
      io:format("Error ~p.\n\n", [Reason]),
      {error}
  end.
