defmodule Foo do
  def say(message) do
    IO.puts message
  end
end

# This is a comment

Foo.say("Hello World!")
