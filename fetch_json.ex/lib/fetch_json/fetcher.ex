defmodule FetchJson.Fetcher do
  def main(_args) do
    :random.seed(:os.timestamp)
    fetch |> parse |> report
  end

  defp fetch() do
    url = random_url
    IO.puts "Fetching #{url}"
    HTTPotion.get(url)
  end

  defp parse(%HTTPotion.Response{status_code: 200, body: body}) do
    {:ok, data} = JSX.decode(body)
    {:ok, inspect(data)}
  end
  defp parse(%HTTPotion.Response{status_code: 404, body: body}) do
    {:error, "Not found, body: #{body}"}
  end
  defp parse(%HTTPotion.Response{status_code: status_code, body: body}) do
    {:error, "status: #{status_code}, body: #{body}"}
  end
  defp parse(response) do
    {:error, inspect(response)}
  end

  defp report({:ok, body}) do
    IO.puts body
  end
  defp report({:error, reason}) do
    IO.puts "Error: #{reason}"
  end

  defp random_url do
    urls = [
      "https://catalogue.bookrepublic.it/promotions/daily.json", # OK
      "https://catalogue.bookrepublic.it/doesnt_exist.json", # 404
      "https://catalogue.bookrepublic.it", # 403 Forbidden
      "https://catalogue.bookrepublic.it/promotions.html", # 406 Not Acceptable
    ]
    index = :random.uniform * length(urls) |> trunc
    Enum.fetch!(urls, index)
  end
end
